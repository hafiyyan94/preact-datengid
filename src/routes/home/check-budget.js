import WhyUs2 from "./why-us-2";
import style from './style';

const CheckBudget = () => (
    <div class="why_us_area primary-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section_title text-center mb-90">
                        <h3 class="primary-text wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: fadeInUp;">Sesuaikan Kebutuhan dengan Budgetmu</h3>
                        <p class="text-white wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s;font-size: 24px;">Pilih fitur yang kamu mau sesuai kebutuhan dan budgetmu</p>
                        <br /><a href="" class="btn " style="padding: 15px; background-color: #ffa827; border-color: #ffa827;" onclick={modalHere.bind(this)} >Simulasi Budget</a>
                    </div>
                </div>
            </div>
        </div>
        <ModalCheckBudget />
    </div>
);

function ModalCheckBudget() {
    const fixed_input_names = [
        { 'id': 'initial', 'value': 'initial', 'url': "/assets/images/feature/inisial.svg" },
        { 'id': 'nama', 'value': 'nama', 'url': "/assets/images/feature/nama.svg" },
        { 'id': 'alamat', 'value': 'alamat', 'url': "/assets/images/feature/alamat.svg" },
        { 'id': 'tanggal_acara', 'value': 'tanggal acara', 'url': "/assets/images/feature/tanggal.svg" },
    ];

    const input_names = [
        { 'id': 'cerita_singkat', 'value': 'Cerita Singkat', 'url': "/assets/images/feature/cerita-singkat.svg" },
        { 'id': 'countdown', 'value': 'Countdown', 'url': "/assets/images/feature/countdown-acara.svg" },
        { 'id': 'galeri_foto', 'value': 'Galeri Foto', 'url': "/assets/images/feature/galeri-foto.svg" },
        { 'id': 'peta_lokasi', 'value': 'Peta Lokasi', 'url': "/assets/images/feature/peta-lokasi.svg" },
        { 'id': 'musik_latar', 'value': 'Musik Latar', 'url': "/assets/images/feature/musik-latar.svg" },
        { 'id': 'ucapan_selamat', 'value': 'Ucapan Selamat', 'url': "/assets/images/feature/ucapan-selamat.svg" },
    ];

    return (
        <div id="modal-checkbudget" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                <div class={"modal-content " + style.modalcorners}>
                    <div class="modal-body primary-bg">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick={closeModal.bind(this)}>
                            <span aria-hidden="true" class="text-white">&times;</span>
                        </button>
                        <br />
                        <div class="row justify-content-end primary-bg">
                            <div class="col-12 col-sm-6">
                                <h3 class="secondary-text text-center" style="padding: 15px;">Pilih Fitur Undangan</h3>
                                {CardInputDisabled(fixed_input_names)}
                                <br />
                                {CardInput(input_names.slice(0, 3), cardOnClick)}
                                {CardInput(input_names.slice(3, 6), cardOnClick)}
                                <br />
                                <p style="font-size: 14px">*fitur inisial, nama, alamat, dan tanggal termasuk dalam fitur basic dan tidak dapat dinon-aktifkan</p>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="row" style="padding: 15px;">
                                    {ListFeatureFixed()}
                                </div>
                                <div class="row" style="padding: 15px;">
                                    {ListFeatureAdditional(input_names, true)}
                                </div>
                                <div class="row" style="padding: 15px;">
                                    <div class="col">
                                        <h5 class="secondary-text">Total Harga </h5>
                                    </div>
                                    <div class="col text-right">
                                        <h3 class="secondary-text" style="display: inline;">Rp</h3>
                                        <h1 class="secondary-text" id="price-sum" style="display: inline;">150</h1>
                                        <h1 class="secondary-text" style="display: inline;">.000</h1>
                                    </div>

                                </div>
                                <div class="row justify-content-center" style="padding: 15px;">
                                    {/* <a href="" class="btn btn-danger" style="padding: 5px; margin: 10px; width: 150px;" onclick={resetPrice.bind(this)} >Reset</a> */}
                                    <a href="" class="btn btn-success" style="padding: 5px; margin: 10px; width: 150px;" >Buat Undangan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const ListFeatureFixed = () => {
    return (
        <div class="card d-flex flex-row justify-content-between " style="width: 100%;">
            <div class="list-group w-75">
                <div class="list-group-item align-items-center">Inisial</div>
                <div class="list-group-item align-items-center">Nama</div>
                <div class="list-group-item align-items-center">Alamat</div>
                <div class="list-group-item align-items-center">Tanggal Acara</div>
            </div>
            <div class="list-group d-flex w-25">
                <div class="list-group-item h-100 text-nowrap text-center">150.000</div>
            </div>
        </div>
    );
}

const ListFeatureAdditional = (inputs, is_hidden) => {
    return (
        <div class="card justify-content-between " style="width: 100%;">
            {inputs.map((input) => {
                return (
                    <div id={input['id'] + "-card"} class="card justify-content-between" style="width: 100%;" hidden={is_hidden}>
                        <div class="list-group d-flex flex-row">
                            <div class="list-group-item align-items-center w-75">{input['value']}</div>
                            <div class="list-group-item align-items-center w-25 text-nowrap text-center">20.000</div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

function closeModal() {
    $('#modal-checkbudget').modal('hide');
}

function modalHere(event) {
    event.preventDefault();
    $('#modal-checkbudget').modal('show');
}

const CardInput = (inputs, click_function = function () { }) => {
    const additional_class = "";

    return (
        <div class="card-deck" style="padding: 0px 15px;">

            {inputs.map((input) =>
                ClickableCard(input['id'], click_function, additional_class, input['value'], input['url'])
            )}

        </div>
    );
}

const CardInputDisabled = (fixed_inputs, click_function = function () { }) => {
    const additional_class = style.item_card_selected + " " + style.item_card_disabled;

    return (
        <div class="card-deck" style="padding: 10px 15px;">

            {fixed_inputs.map((input) =>
                ClickableCard(input['id'], click_function, additional_class, input['value'], input['url'])
            )}

        </div>
    );
}

const ClickableCard = (element_id, click_function, additional_class, element_value, image_url = "") => {
    const card_class = "card " + style.item_card + " " + additional_class

    return (
        <div id={element_id} class={card_class} data-selected="0">
            <div onClick={click_function.bind(this)} data-parent={element_id} style="padding: 10px; width: 100%; height: 100%;">
                <div class={"img-fluid " + style.item_image} style={"background-image: url(" + image_url + ");"} data-parent={element_id}></div>
                <div class="card-body text-center" style="padding: 0; margin-top: 5px;" data-parent={element_id}>
                    <h5 id={element_id + "-text"} class="card-title" style="margin:0;" data-parent={element_id}>{element_value}</h5>
                </div>
            </div>
        </div>
    );
}

function cardOnClick(event) {
    const parent_id = event.target.dataset['parent'];
    const parent = document.getElementById(parent_id);
    const text = document.getElementById(parent_id + '-text');
    const item_card_selected_class = style.item_card_selected
    if (parseInt(parent.dataset['selected'])) {
        parent.dataset['selected'] = '0';
        parent.classList.remove(item_card_selected_class);
        changeSum(parent_id, true);
    } else {
        parent.dataset['selected'] = '1';
        parent.classList.add(item_card_selected_class);
        changeSum(parent_id, false);
    }
}

function changeSum(element_id, is_hidden) {
    const price_element = document.getElementById("price-sum");
    let price_sum = parseInt(document.getElementById("price-sum").textContent);

    if (is_hidden) {
        price_sum -= 20;
    } else {
        price_sum += 20;
    }

    price_element.innerText = price_sum;

    let card_id = element_id + "-card";
    const card_element = document.getElementById(card_id);
    card_element.hidden = is_hidden
}

function resetPrice(event) {
    event.preventDefault();
    document.getElementById("price-sum").innerText = '150';
    const price_inputs = document.getElementsByClassName("price-input");
    for (let index = 0; index < price_inputs.length; index++) {
        price_inputs[index].checked = false;

    }
}

export default CheckBudget;