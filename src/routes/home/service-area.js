const ServiceArea = () => (
  <div class="service_area primary-bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="text-center">
            <h3 class="wow fadeInUp primary-text" data-wow-duration="1.2s" data-wow-delay=".2s">Mulai Buat Undangan</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ServiceArea;