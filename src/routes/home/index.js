import SliderTop from './slider-top';
import AboutUs from './about-us';
import WhyUs1 from './why-us-1';
import WhyUs2 from './why-us-2';
import WhyUs3 from './why-us-3';
import WhyUs4 from './why-us-4';
import CheckBudget from './check-budget';
import WhyUs5 from './why-us-5';
const Home = () => (
	<div>
		<SliderTop />
		<AboutUs />
		<WhyUs1 />
		<WhyUs2 />
		<WhyUs3 />
		<WhyUs4 />
		<CheckBudget />
		<WhyUs5 />
	</div>
);

export default Home;
