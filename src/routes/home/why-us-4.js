import { useState } from 'preact/hooks';

const DESC_KEY = "desc";
const TITLE_KEY = "title";

const descriptionText = {
    "none": { "title": "", "desc": "" },
    "cerita-singkat": { "title": "Cerita Singkat", "desc": "Fitur ini menyediakan gambaran dan cerita singkat dari perjalanan cinta kedua mempelai." },
    "countdown-acara": { "title": "Countdown Acara", "desc": "Fitur ini menyediakan perhitungan waktu mundur sampai hari H." },
    "galeri-foto": { "title": "Galeri Foto", "desc": "Fitur ini menyediakan galeri-galeri foto kedua mempelai (ex. Foto prawed, foto saat lamaran, foto saat pacaran, dll." },
    // "hadiah-online": {"title": "Galeri Foto", "desc": "Fitur Hadiah Online"},
    "musik-latar": { "title": "Galeri Foto", "desc": "Fitur ini menyediakan musik latar dalam undangan online yang dapat menggambarkan suasana bahagia kedua mempelai. (musik dapat dipilih sesuai keinginan)" },
    "peta-lokasi": { "title": "Galeri Foto", "desc": "Fitur Peta Lokasi" },
    "ucapan-selamat": { "title": "Galeri Foto", "desc": "Fitur Ucapan Selamat" },
    // "undangan-spesial": {"title": "Galeri Foto", "desc": "Fitur Undangan Tamu Spesial"},
    // "buku-tamu-qrcode": {"title": "Galeri Foto", "desc": "Fitur Buku Tamu QR Code"},
}

const spotlightElement = (featureName) => {

    if (featureName === 'none')
        return ('');
    else
        return (
            <div class="team_area wow fadeInUp" style="padding-top:70px;padding-bottom: 0;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="single_team wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
                                <div class="team_thumb">
                                    <img class="img-fluid mx-auto d-block img-spotlight" src={"/assets/images/feature/" + featureName + ".svg"} alt="" />
                                </div>
                                <div class="team_title orange-bg spotlight-text" style="padding:10px;">
                                    <div class="container">
                                        <h4 class="text-white text-bold mx-auto d-block">{descriptionText[featureName][TITLE_KEY]}</h4>
                                        <p class="text-white mx-auto d-block">{descriptionText[featureName][DESC_KEY]}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
}

const WhyUs4 = () => {

    const [selectedFeature, setSelectedFeature] = useState("none");
    const [showSelectedFeature, setShowSelectedFeature] = useState(false);

    const describe = (eventObject) => {

        if (selectedFeature === eventObject.target.id) {
            setShowSelectedFeature(!showSelectedFeature);
            setSelectedFeature("none");
        } else {
            setShowSelectedFeature(true);
            setSelectedFeature(eventObject.target.id);
        }
    }

    return (
        <div class="feature-area orange-bg">
            <div class="container-fluid primary-bg">
                <div class="row justify-content-end">
                    <div class="col-12 col-sm-6">
                        <div class="section_title wow fadeInUp mb-30" style="padding-top:50px;">
                            <h3 class="secondary-text text-center">Tambahkan Fitur &amp; Buat Undangan Semakin Menarik</h3>
                        </div>
                        <div class="container wow fadeInUp">
                            <div class="row mb-30">
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="cerita-singkat" onClick={event => describe(event)} src="/assets/images/feature/cerita-singkat.svg" alt="" />
                                    <p>Cerita Singkat</p>
                                </div>
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="countdown-acara" onClick={event => describe(event)} src="/assets/images/feature/countdown-acara.svg" alt="" />
                                    <p>Countdown Acara</p>
                                </div>
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="galeri-foto" onClick={event => describe(event)} src="/assets/images/feature/galeri-foto.svg" alt="" />
                                    <p>Galeri Foto</p>
                                </div>
                                {/* <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="hadiah-online" onClick={event => describe(event)}  src="/assets/images/feature/hadiah-online.svg" alt=""/>
                                    <p>Hadiah Online</p>
                                </div> */}
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="musik-latar" onClick={event => describe(event)} src="/assets/images/feature/musik-latar.svg" alt="" />
                                    <p>Musik Latar</p>
                                </div>
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="peta-lokasi" onClick={event => describe(event)} src="/assets/images/feature/peta-lokasi.svg" alt="" />
                                    <p>Peta Lokasi</p>
                                </div>
                                {/* <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="buku-tamu-qrcode" onClick={event => describe(event)}  src="/assets/images/feature/buku-tamu-qrcode.svg" alt=""/>
                                    <p>Buku Tame QR Code</p>
                                </div> */}
                                <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="ucapan-selamat" onClick={event => describe(event)} src="/assets/images/feature/ucapan-selamat.svg" alt="" />
                                    <p>Ucapan Selamat</p>
                                </div>
                                {/* <div class="col-6 col-md-4 mb-30 feature-card">
                                    <img id="undangan-spesial" onClick={event => describe(event)}  src="/assets/images/feature/undangan-spesial.svg" alt=""/>
                                    <p>Undangan Tamu Spesial</p>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 wow fadeInUp">
                        {spotlightElement(selectedFeature)}
                    </div>
                </div>
            </div>
        </div>
    );
}
export default WhyUs4;