import WhyUs1 from "./why-us-1";
import style from "./style";

const WhyUs2 = () => (
<div class="why_us_area light-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section_title text-center mb-90">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Buat dan Sebarkan Undanganmu <br /> Hanya dalam 4 Langkah</h3>
        </div>
      </div>
    </div>
    <div class={"row " +style.container_4_steps}>
      <div class="item-1 mb-30 wow fadeInLeft">
        <div class="col-12">
          <h3 class={"bold-text "+style.text_datengId_color}>Pilih Desain Yang Kamu Mau</h3>
        </div>
        <div class="row half-left-border">    
          <div class="col-2 col-xl-1">
            <h2 class="boxed">1</h2>
          </div>
          <div class="col-10 col-xl-11">
            <p class="feature-text dark-text">
              Pastikan untuk memilih desain yang kamu mau sesuai dengan yang kami sediakan atau sesuai dengan kreasimu sendiri. Jangan lupa untuk mengisikan informasi-informasi lengkap yang ingin kamu tampilkan dalam undangan.
            </p>
          </div>
        </div>
      </div>

      <div class="item-2 mb-30 wow fadeInRight">
        <div class="row">
          <div class="col-12">
            <h3 class={"bold-text text-right pr-10 "+style.text_datengId_color}>Tambahkan Fitur Sesuai Kebutuhanmu </h3>
          </div>
        </div>
        <div class="row half-right-border">
          <div class="col-10 col-xl-11">
            <p class="feature-text dark-text text-right">
              Kamu bisa menambahkan fitur-fitur buat undanganmu. Fitur- yang kami sediakan cukup beragam yang dapat membuat <br /> undanganmu semakin menarik tentunya.
            </p>
          </div>
          <div class="col-2 col-xl-1">
            <h2 class="boxed">2</h2>
          </div>
        </div>
      </div>

      <div class="item-3 mb-30 wow fadeInLeft">
        <div class="row">
          <div class="col-12">
            <h3 class={"bold-text pl-30 "+style.text_datengId_color}>Pesan dan Bayar</h3>
          </div>
        </div>
        <div class="row half-left-border">
          <div class="col-2 col-xl-1">
            <h2 class="boxed">3</h2>
          </div>
          <div class="col-10 col-xl-11">
            <p class="feature-text dark-text">
              Jika undangan telah selesai kamu buat, pastikan untuk mengisi form pemesanan dan lakukan pembayaran. Sebelum melakukan pesanan pastikan udanganmu telah sesuai dengan keinginanmu ya.
            </p>
          </div>
        </div>
      </div>
      <div class="item-4 wow fadeInRight">
      <div class="row">
        <div class="col-12">
          <h3 class={"bold-text text-right pr-10 "+style.text_datengId_color}>Sebarkan Undanganmu</h3>
        </div>
      </div>
      <div class="row half-right-border">
        <div class="col-10 col-xl-11">
          <p class="feature-text dark-text text-right">
            Jika semua langkah di atas telah kamu lakukan, selanjutnya kamu akan menerima alamat undangan mu yang bisa kamu sebarkan ke teman, kerabat serta keluargamu. Yuk, sebarkan !
          </p>
        </div>
        <div class="col-2 col-xl-1">
          <h2 class="boxed">4</h2>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
);

export default WhyUs2;