function TemplateCarousel() {
    return (

        <div id="carouselIndicators" class="carousel slide" data-interval="false">

            <CarouselIndicators />

            <CarouselInner />

            <a class="carousel-control-prev" style="filter: invert(100%);" href="#carouselIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true" />
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" style="filter: invert(100%);" href="#carouselIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true" />
                <span class="sr-only">Next</span>
            </a>

        </div>

    );
}

function CarouselIndicators() {
    return (
        <ol class="carousel-indicators" style="filter: invert(100%);">
            <li data-target="#carouselIndicators" data-slide-to="0" class="active" />
            <li data-target="#carouselIndicators" data-slide-to="1" />
            <li data-target="#carouselIndicators" data-slide-to="2" />
        </ol>
    );
}

function CarouselInner() {
    return (
        <div class="carousel-inner" style="padding: 0 5%;">
            <div class="carousel-item active">{CarouselItem(0, 1, 2)}</div>
            <div class="carousel-item">{CarouselItem(3, 4, 5)}</div>
            <div class="carousel-item">{CarouselItem(6, 7, 8)}</div>
        </div>
    );
}

function CarouselItem(index1, index2, index3) {

    const modalContent = [
        { name: "Dreamwed", image_link: "/assets/images/thumbnails/dreamwed.jpg", preview_link: "https://dreamwed.dateng.id" },
        { name: "Glint", image_link: "/assets/images/thumbnails/glint.jpg", preview_link: "https://glint.dateng.id" },
        { name: "Hookup", image_link: "/assets/images/thumbnails/hookup.jpg", preview_link: "https://hookup.dateng.id" },
        { name: "Nissa", image_link: "/assets/images/thumbnails/nissa.jpg", preview_link: "https://nissa.dateng.id" },
        { name: "Shutter", image_link: "/assets/images/thumbnails/shutter.jpg", preview_link: "https://shutter.dateng.id" },
        { name: "Snapshot", image_link: "/assets/images/thumbnails/snapshot.jpg", preview_link: "https://snapshot.dateng.id" },
        { name: "Sunshine", image_link: "/assets/images/thumbnails/sunshine.jpg", preview_link: "https://sunshine.dateng.id" },
        { name: "Invits", image_link: "/assets/images/thumbnails/invits.jpg", preview_link: "https://invits.dateng.id" },
        { name: "Wed", image_link: "/assets/images/thumbnails/wed.jpg", preview_link: "https://wed.dateng.id" },
    ];

    const carouselTemplateItem = (modalContentIndex) => {
        return (
            <div class="col-lg-4 col-md-4 align-content-center" style="padding: 5px">
                <div class="card">
                    <div style="display: block; height: 215px; overflow: hidden;">
                        <img class="card-img-top" src={modalContent[modalContentIndex]["image_link"]} alt="Card image cap" />
                    </div>
                    <div class="card-body primary-bg text-center" style="padding: 10px;">
                        <h5 class="card-title text-white no-margin" style="margin: 0 .375rem;">{modalContent[modalContentIndex]["name"]}</h5>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div class="row justify-content-center" style="padding: 50px 75px">

            {carouselTemplateItem(index1)}

            {carouselTemplateItem(index2)}

            {carouselTemplateItem(index3)}

        </div>
    );
}

export default TemplateCarousel;