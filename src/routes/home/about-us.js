import style from './style';
import TemplateCarousel from './carousel';



const AboutUs = () => (
  <div class={style.about_us_custom + " primary-bg"}>
    <div class="container">
      <div class="row centered wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
        <div class="row justify-content-center">
          {ModalButton("modal-mausimpel", "/assets/images/feature/mau-yang-simpel.png")}
          {ModalButton("modal-cobakreasimu", "/assets/images/feature/coba-kreasimu.png")}
        </div>
      </div>
    </div>
    <ModalCobaKreasimu />
    <ModalMauSimpel />
  </div>
);

function ModalButton(target_id, target_image) {
  return (
    <div class={"col-md-5 align-content-center " + style.rcorners} data-toggle="modal" data-target={"#" + target_id} >
      <img class={"mx-auto d-block " + style.imageAboutUs} src={target_image} alt="" />
      <span class="mx-auto d-block"><h4 class="text-white text-center">Coba kreasimu</h4></span>
    </div>
  );
}


function ModalCobaKreasimu() {
  return (
    <div id="modal-cobakreasimu" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class={"modal-content " + style.modalcorners}>
          <div class="modal-body primary-bg">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="text-white">&times;</span>
            </button>
            <br />
            <div class="row primary-bg">
              <div class="col-lg-12 col-md-12">

                <div class="row primary-bg justify-content-center" style="padding: 25px 0px 50px">
                  <div class="col-lg-8 col-md-8 align-content-center">
                    <h2 class="text-center primary-text">Undangan Versi Kamu</h2>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-4 col-md-4 align-content-center">
                    <div class="row justify-content-center ml-2 mr-2">
                      <img class={"mx-auto d-block " + style.imageCobaKreasi} src="/assets/images/example/example-1.png" alt="" />
                      <span class="mx-auto d-block"><h3 class="text-white text-center">Preview 1</h3></span>
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <p class="text-white" style="font-size: 1.7em; line-height: 1.5em;/*! word-spacing: 1.0em; */">
                      Kami menyediakan pembuatan undangan online Versi Kamu. Tentunya dengan beberapa persyaratan yang perlu kamu siapkan
                    </p>
                  </div>
                </div>
                <div class="row mt-10">
                  <div class="col-lg-4 col-md-4 align-content-center">
                    <div class="row justify-content-center ml-2 mr-2">
                      <img class={"mx-auto d-block " + style.imageCobaKreasi} src="/assets/images/example/example-2.png" alt="" />
                      <span class="mx-auto d-block"><h3 class="text-white text-center">Preview 2</h3></span>
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <p class="text-white" style="font-size: 1.7em; line-height: 1.5em;/*! word-spacing: 1.0em; */">
                      Persyaratan ini tentunya akan berbeda dari undangan dengan template yang kami sediakan. Jika Berminat silahkan ajukan undangan Versi Kamu ke admin kami di email admin@dateng.id.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function ModalMauSimpel() {
  return (
    <div id="modal-mausimpel" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class={"modal-content " + style.modalcorners}>
          <div class="modal-body primary-bg">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="text-white">&times;</span>
            </button>
            <br />
            <div class="row light-bg">
              <div class="col-lg-12 col-md-12">

                <div class="row primary-bg justify-content-center" style="padding: 25px 0px 50px">
                  <div class="col-lg-8 col-md-8 align-content-center">
                    <h2 class="text-center primary-text">Kami Menyediakan Banyak Pilihan Template yang Bisa Dipilih</h2>
                  </div>
                </div>

                <TemplateCarousel />

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function LoremIpsum() {
  return (
    <p class="text-white">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tincidunt lectus vitae turpis maximus suscipit. In posuere luctus convallis. Nulla facilisi. Nunc quis nisi nulla. Etiam et hendrerit turpis. Nullam sit amet tellus lorem. Quisque eget pulvinar arcu. Nulla consequat scelerisque risus, vitae laoreet urna malesuada a. Fusce quis dictum tellus. Nullam consectetur lectus neque, eget malesuada urna suscipit eget. Morbi vestibulum efficitur turpis eu sagittis. Sed sit amet condimentum sem.
    </p>
  );
}
export default AboutUs;