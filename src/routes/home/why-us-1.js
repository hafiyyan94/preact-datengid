const WhyUs1 = () => (
<div class="why_us_area primary-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section_title mb-50">
          <h3 class="wow fadeInUp text-white" data-wow-duration="1s" data-wow-delay=".3s">Kenapa harus di Dateng.id</h3>
        </div>
      </div>
    </div>
    <div class="row mb-50">
      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-3">
            <img class="img-fluid" src="/assets/images/icon/easy.png" alt=""/>
          </div>
          <div class="col-9 align-middle">
            <h4 class="text-white">Buatnya Gampang</h4>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12">
            <p class="light-text">
            Cukup dengan mengakses dateng.id, kamu bisa bikin undangan versi kamu.
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-3">
            <img class="img-fluid" src="/assets/images/icon/quick.png" alt=""/>
          </div>
          <div class="col-9 align-middle">
            <h4 class="text-white">Jadinya Cepat</h4>
          </div>
        </div>
          
        <div class="row">
          <div class="col-12">
            <p class="light-text">
            Di dateng.id, kamu bisa bikin undangan versi kamu hanya dalam waktu 5 hari.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-3">
            <img class="img-fluid" src="/assets/images/icon/budget.png" alt=""/>
          </div>
          <div class="col-9 align-middle">
            <h4 class="text-white">Sesuaikan Budget</h4>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12">
            <p class="light-text">
            Takut budget ga sesuai harapan? Tenang aja, di dateng.id kamu tinggal pilih fitur-fitur yang pas dikantong.
            </p>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-3">
            <img class="img-fluid" src="/assets/images/icon/share.png" alt=""/>
          </div>
          <div class="col-9 align-middle">
            <h4 class="text-white">Nyebarinnya Mudah</h4>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12">
            <p class="light-text">
            Kamu bisa kirim-kirim undangan kamu cuma dengan handphone loh, karena websitenya udah disediain sama Dateng.id. Jadi tinggal KLIK!
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
);

export default WhyUs1;