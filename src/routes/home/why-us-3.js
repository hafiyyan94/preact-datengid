import WhyUs2 from "./why-us-2";

const WhyUs3 = () => (
<div class="why_us_area primary-bg">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <div class="section_title text-center mb-90">
          <h3 class="primary-text wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: fadeInUp;">Cukup dengan 150 Ribu </h3>
          <p class="text-white wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s;font-size: 24px;">4 fitur utama yang bisa kamu dapatkan</p>
        </div>
      </div>
    </div>
    <div class="row mb-50">
      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-12 col-sm-4 text-center mb-20">
            <img class="img-fluid" src="/assets/images/feature/inisial.svg" alt=""/>
          </div>
          <div class="col-12 col-sm-8 align-middle">
            <h4 class="primary-text">Inisial</h4>
            <p class="light-text textbox">
              Inisial kedua mempelai atau nama panggilan kedua mempelai
            </p>
          </div>
        </div>  
      </div>

      <div class="col-12 col-md-6 wow wow fadeInUp">
        <div class="row mb-20">
          <div class="col-12 col-sm-4 text-center mb-20">
              <img class="img-fluid" src="/assets/images/feature/alamat.svg" alt=""/>
          </div>
          <div class="col-12 col-sm-8 align-middle">
            <h4 class="primary-text">Alamat</h4>
            <p class="light-text textbox">
              Informasi nama lengkap serta informasi mengenai keluarga kedua mempelai
            </p>
          </div>
        </div> 
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
            <div class="col-12 col-sm-4 text-center mb-20">
              <img class="img-fluid" src="/assets/images/feature/nama.svg" alt=""/>
            </div>
            <div class="col-12 col-sm-8 align-middle">
              <h4 class="primary-text">Nama</h4>
              <p class="light-text textbox">
                Informasi nama lengkap serta informasi mengenai keluarga kedua mempelai 
              </p>
            </div>
        </div>  
      </div>

      <div class="col-12 col-md-6 wow fadeInUp">
        <div class="row mb-20">
          <div class="col-12 col-sm-4 text-center mb-20">
            <img class="img-fluid" src="/assets/images/feature/tanggal.svg" alt=""/>
          </div>
          <div class="col-12 col-sm-8 align-middle">
            <h4 class="primary-text">Tanggal Acara</h4>
            <p class="light-text textbox">
              Informasi nama lengkap serta informasi mengenai keluarga kedua mempelai 
            </p>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
);

export default WhyUs3;