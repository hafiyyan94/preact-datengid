import WhyUs4 from "./why-us-4";
import style from "./style";

const form_url = 'https://docs.google.com/forms/d/e/1FAIpQLSc_s0azElW4pkj-EUEXugYhsXyxmYnKgBsDORkGok27tJLrtg/viewform?usp=sf_link';

const WhyUs5 = () => (
  <div data-scroll-index="0" class={style.footer_datengId_custom + " " + style.mockup_image}>
    <div class="container">
      <div class="row">
        <div class={"col-lg-5"}></div>
        <div class="col-lg-7">
          <div class="section_title mb-90">
            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Sudah tahu caranya kan ?</h3>
            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Buat Undanganmu, Yuk!!</h3>
          </div>
          <a href={form_url} class="btn btn-success" style="width:200px; padding: 20px;">Buat Sekarang</a>
        </div>
      </div>
    </div>
  </div>
);

export default WhyUs5;