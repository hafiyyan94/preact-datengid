const SliderTop = () => (
  <div class="slider_area">
    <div class="single_slider  d-flex align-items-center slider_bg_1">
      <div class="container">
        <div class="row align-items-center justify-content-start">
          <div class="col-lg-10 col-md-10">
            <div class="slider_text">
              <h3 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">
                  Buat Undangan <br /> Online
              </h3>
              <h3 class="wow fadeInLeft" style="font-size: 33px;">Mudah dan Cepat</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default SliderTop;