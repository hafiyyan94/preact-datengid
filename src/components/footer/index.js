const Footer = () => (
	<footer class="footer">
		<div class="footer_top">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-2 col-md-3">
						<div class="footer_logo wow fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">
							<a href="index.html">
									<img src="/assets/images/logo.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="col-xl-8 col-lg-7 col-md-9">
						<div class="menu_links">
							<ul>
								<li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s" href="#">Halaman Muka</a></li>
								<li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s" href="#">Undangan</a></li>
								<li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s" href="#">Tentang Kami</a></li>
								<li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".8s" href="#">Hubungi Kami</a></li>
								<li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s" href="#">Daftar</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-12">
						<div class="socail_links">
							<ul>
								<li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" href="#"> <i class="fa fa-facebook"></i> </a></li>
								<li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" href="#"> <i class="fa fa-twitter"></i> </a></li>
								<li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" href="#"> <i class="fa fa-instagram"></i> </a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-right_text">
			<div class="container">
				<div class="footer_border"></div>
				<div class="row">
					<div class="col-xl-12">
						<p class="copy_right text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
);

export default Footer;