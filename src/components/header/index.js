const form_url = 'https://docs.google.com/forms/d/e/1FAIpQLSc_s0azElW4pkj-EUEXugYhsXyxmYnKgBsDORkGok27tJLrtg/viewform?usp=sf_link';

const Header = () => (
  <header>
    <div class="header-area dark-bg">
      <div id="sticky-header" class="main-header-area">
        <div class="container-fluid p-0">
          <div class="row align-items-center no-gutters">
            <div class="col-xl-2 col-lg-2">
              <div class="logo-img">
                <a href="index.html">
                  <img src="/assets/images/logo.png" alt="" />
                </a>
              </div>
            </div>
            <div class="col-xl-10 col-lg-10">
              <div class="main-menu  d-none d-lg-block text-right">
                <nav>
                  <ul id="navigation">
                    {/* <li><a class="active" href="index.html">Halaman Muka</a></li>
                    <li><a href="#">Undangan</a></li>
                    <li><a href="#">Tentang Kami</a></li>
                    <li><a href="#">Hubungi Kami</a></li> */}
                    <li><a href="#" style="visibility: hidden;">|</a></li>
                    <li><a href={form_url} class="btn btn-green text-nowrap" style="width: auto; padding: 0px 15px">Buat Undangan</a></li>
                  </ul>
                </nav>
              </div>
            </div>
            <div class="col-12">
              <div class="mobile_menu d-block d-lg-none"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
);

export default Header;
